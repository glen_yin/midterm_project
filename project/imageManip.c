// Nathaniel Eisenberg neisenb3
// Glen Yin jyin19
// imageManip.c
// 601.220, Spring 2019

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "imageManip.h"
#include "ppm_io.h"

#define sq(x) ((x)*(x))
#define PI acos(-1.0)

// swaps the rbg values
void swap(Image* img) {

  for (int i = 0; i < img->rows; i++) {
    for (int j = 0; j < img->cols; j++) {
      unsigned char temp; // temperary value to hold the red value so it can be changed to the green value
      temp = img->data[j + i*img->cols].r;
      img->data[j + i*img->cols].r = img->data[j + i*img->cols].g;
      img->data[j + i*img->cols].g = img->data[j + i*img->cols].b;
      img->data[j + i*img->cols].b = temp;
    }
  }
}

// alter the brightness of the image
void bright(Image* img, int value) {

  for (int i = 0; i < img->rows; i++) {
    for (int j = 0; j < img->cols; j++) {

      int index = j + i*img->cols; //index of the data array (affects each pixel in the image)

      if ((img->data[index].r + value <= 255) && (img->data[index].r + value >= 0)) { //ensures that once the factor is added the rgb value won't leave the [0,255] range.
        img->data[index].r = img->data[index].r + value;
      }
      else if (img->data[index].r + value > 255) {
        img->data[index].r = 255;
      }
      else {
        img->data[index].r = 0;
      }

      if ((img->data[index].g + value <= 255) && (img->data[index].g + value >= 0)) {
        img->data[index].g = img->data[index].g + value;
      }
      else if ((img->data[index].g + value) > 255) {
        img->data[index].g = 255;
      }
      else {
        img->data[index].g = 0;
      }

      if ((img->data[index].b + value <= 255) && (img->data[index].b + value >= 0)) {
        img->data[index].b = img->data[index].b  + value;
      }
      else if ((img->data[index].b + value) > 255) {
        img->data[index].b = 255;
      }
      else {
        img->data[index].b = 0;
      }
    }
  }
}

// inverts the image by subracting its color value from 255
void invert (Image* img) {
  for (int i = 0; i < img->rows; i++) {
    for (int j = 0; j < img->cols; j++) {
      img->data[j + i*img->cols].r = 255 - img->data[j + i*img->cols].r;
      img->data[j + i*img->cols].g = 255 - img->data[j + i*img->cols].g;
      img->data[j + i*img->cols].b = 255 - img->data[j + i*img->cols].b;

    }
  }
}

// grays the image by setting each rbg value to a certain weighted intensity
void gray(Image* img) { 
  for (int i = 0; i < img->rows; i++) {
    for (int j = 0; j < img->cols; j++) {
      double intensity = (0.30*(img->data[j + i*img->cols].r)) + (0.59*(img->data[j + i*img->cols].g)) + (0.11*(img->data[j + i*img->cols].b));
      img->data[j + i*img->cols].r = intensity;
      img->data[j + i*img->cols].g = intensity;
      img->data[j + i*img->cols].b = intensity;

    }
  }
}

// crops the image given the cordinates of top left and bottom right corner
void crop(Image* img, int tc, int tr, int bc, int br) {

  int origcol = img->cols;
  img->cols = bc - tc;
  img->rows = br - tr;
  Pixel *croppix = malloc(sizeof(Pixel) * img->cols * img->rows);
  if (!croppix) {
    fprintf(stderr, "Error, cropped image allocation failed\n");
    return;
  }
  for (int i = 0; i < img->rows; i++) {
    for (int j = 0; j < img->cols; j++) {
      croppix[j + i*img->cols] = img->data[tc + j + (tr + i)*origcol];
    }
  }
  free(img->data);
  img->data = croppix;
}

// blur the image using gaussian matrix with given standard deviation
void blur(Image* img, double sig) {
  int N = sig * 10;
  if (N % 2 == 0) {
    N++;
  }
  double *gaus = malloc(sizeof(double) * sq(N));
  if (!gaus) {
    fprintf(stderr, "Error, gauss matrix allocation failed\n");
    return;
  }
  int k = 0;
  for (int i = -N/2; i <= N/2; i++) {
    for (int j = -N/2; j <= N/2; j++) {
      gaus[k] = (1.0 / (2.0 * PI * sq(sig))) * exp(-(sq(j) + sq(i)) / (2.0 * sq(sig)));
      k++;
    }
  }
  assert(k == sq(N));
  Pixel *blurry = malloc(sizeof(Pixel) * img->cols * img->rows);
  if (!blurry) {
    fprintf(stderr, "Error, blurred image allocation failed\n");
    return;
  }
  for (int i = 0; i < img->rows; i++) {
    for (int j = 0; j < img->cols; j++) {
      blurry[j + i*img->cols] = blurPix(img, i, j, gaus, N);
    }
  }
  free(gaus);
  free(img->data);
  img->data = blurry;
}

// return a new Pixel with gaussian matrix applied to
// all adjacent pixels around the given coordinate
Pixel blurPix(Image* img, int i, int j, double *gaus, int N) {

  double red = 0;
  double green = 0;
  double blue = 0;
  double sum = 0;
  for (int m = 0; m < N; m++) {
    for (int n = 0; n < N; n++) {
      if (i+m >= N/2 && j+n >= N/2
          && i+m <= (img->rows)-1+N/2 && j+n <= (img->cols)-1+N/2) {
        red += gaus[n + m * N] * img->data[j+n-N/2 + (i+m-N/2)*img->cols].r;
        green += gaus[n + m * N] * img->data[j+n-N/2 + (i+m-N/2)*img->cols].g;
        blue += gaus[n + m * N] * img->data[j+n-N/2 + (i+m-N/2)*img->cols].b;
        sum += gaus[n + m * N];
      }
    }
  }
  Pixel new_pix = {cvtchar(red/sum), cvtchar(green/sum), cvtchar(blue/sum)};
  return new_pix;
}

// converts a double value, constricted to 0-255, to an unsigned char
unsigned char cvtchar(double val) {
  unsigned char result = 0;
  if (val > 255) {
    result = 255;
  } else if (val < 0) {
    result = 0;
  } else {
    result = val;
  }
  return result;
}

// detects large changes in color intensity in the
// blurred-gray image to only show large changes in color intensity
void edges(Image* img, double sigma , int threshhold) { 

  gray(img); // grays image to only have intensities of rbg values
  blur(img, sigma); // blurs images to prevent false positives


  Pixel *edge = malloc(sizeof(Pixel) * img->cols * img->rows); // creates a new array to prevent overwriting the image with each loop
  if (!edge) {
    fprintf(stderr, "Error, edge image allocation failed\n");
    return;
  }

  Pixel white = {255, 255, 255};
  Pixel black = {0, 0, 0};

  for (int i = 0; i < img->rows; i++) {
    edge[i*img->cols] = white;
    edge[img->rows-1 + i*img->cols] = white;
  }
  for (int j = 0; j < img->rows; j++) {
    edge[j] = white;
    edge[j + (img->cols-1)*img->cols] = white;
  }
  for (int i = 1; i < (img->rows) - 1; i++) { // loops through to find intensity gradients for each pixel
    for (int j = 1; j < (img->cols) - 1; j++) {
      double intensity_x = (img->data[j + (i+1)*img->cols].r - img->data[j + (i-1)*img->cols].r) / 2.0;

      double intensity_y = (img->data[j + 1 + i*img->cols].r - img->data[j - 1 + i*img->cols].r) / 2.0;

      double intense = sqrt(intensity_x * intensity_x + intensity_y * intensity_y);

      if (intense > threshhold) {
        edge[j + i*img->cols] = black;
      } else {
        edge[j + i*img->cols] = white;
      }
    }
  }
  free(img->data); //frees the allocated memory
  img->data = edge;
}
