// Nathaniel Eisenberg neisenb3
// Glen Yin jyin19
// project.c

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"

#include "imageManip.h"


int main(int argc, char *argv[]) {

  if (argc < 3) {
    fprintf(stderr, "Error: Command lacks input file and/or output file\n");
    return 1;
  }

  if (argc < 4) {
    fprintf(stderr, "Error: Command lacks operation name\n");
    return 4;
  }

  char * inputname = argv[1];  // (copy) pointer to the filename
  char * outputname = argv[2];
  char * operation = argv[3];

  FILE* input = fopen(inputname, "rb");
  if (!input) {
    fprintf(stderr, "Error: could not open input file\n");
    return 2;
  }

  Image * im = read_ppm(input);
  if (!im) {
    fprintf(stderr, "Error: could not read input file\n");
    return 3;
  }

  fclose(input);

  if (!strcmp(operation, "swap")) {
    if (argc != 4) {
      fprintf(stderr, "Error: incorrect number of arguments for the function swap\n");
      free(im->data);
      free(im);
      return 5;
    }
    swap(im);
  } else if (!strcmp(operation, "bright")) {
    if (argc != 5) {
      fprintf(stderr, "Error: incorrect number of arguments for the function bright\n");
      free(im->data);
      free(im);
      return 5;
    }
    int mag;
    if(!sscanf(argv[4], "%d", &mag)) {
      fprintf(stderr, "Error: invalid argument\n");
      free(im->data);
      free(im);
      return 6;
    }
    if (mag > 255 || mag < -255) {
      fprintf(stderr, "Error: invalid argument\n");
      free(im->data);
      free(im);
      return 6;
    }
    bright(im, mag);
  } else if (!strcmp(operation, "invert")) {
    if (argc != 4) {
      fprintf(stderr, "Error: incorrect number of arguments for the function invert\n");
      free(im->data);
      free(im);
      return 5;
    }
    invert(im);
  } else if (!strcmp(operation, "gray")) {
    if (argc != 4) {
      fprintf(stderr, "Error: incorrect number of arguments for the function gray\n");
      free(im->data);
      free(im);
      return 5;
    }
    gray(im);
  } else if (!strcmp(operation, "crop")) {
    if (argc != 8) {
      fprintf(stderr, "Error: incorrect number of arguments for the function crop\n");
      free(im->data);
      free(im);
      return 5;
    }
    int topc;
    int topr;
    int botc;
    int botr;
    int parse = sscanf(argv[4], "%d", &topc);
    parse += sscanf(argv[5], "%d", &topr);
    parse += sscanf(argv[6], "%d", &botc);
    parse += sscanf(argv[7], "%d", &botr);
    if (parse != 4) {
      fprintf(stderr, "Error: invalid arguments\n");
      free(im->data);
      free(im);
      return 6;
    }
    if (topc < 0 || topr < 0 || topc >= botc || topr >= botr
        || botc > im->cols || botr > im->rows) {
      fprintf(stderr, "Error: invalid argument\n");
      free(im->data);
      free(im);
      return 6;
    }


    crop(im, topc, topr, botc, botr);

  } else if (!strcmp(operation, "blur")) {
    if (argc != 5) {
      fprintf(stderr, "Error: incorrect number of arguments for the function blur\n");
      free(im->data);
      free(im);
      return 5;
    }
    double sigma;
    if(!sscanf(argv[4], "%lf", &sigma)) {
      fprintf(stderr, "Error: invalid argument\n");
      free(im->data);
      free(im);
      return 6;
    }
    if (sigma == 0) {
      fprintf(stderr, "Error: invalid argument\n");
      free(im->data);
      free(im);
      return 6;
    }
    blur(im, sigma);
  } else if (!strcmp(operation, "edges")) {
    if (argc != 6) {
      fprintf(stderr, "Error: incorrect number of arguments for the function edge-detect\n");
      free(im->data);
      free(im);
      return 5;
    }
    double sigma;
    if(!sscanf(argv[4], "%lf", &sigma)) {
      fprintf(stderr, "Error: invalid argument\n");
      free(im->data);
      free(im);
      return 6;
    }
    double threshhold;
    if (!sscanf(argv[5], "%lf", &threshhold)) {
      fprintf(stderr, "Error: invalid argument");
      free(im->data);
      free(im);
    }

    edges(im, sigma, threshhold);

  } else {
    fprintf(stderr, "Error: operation name is invalid\n");
    free(im->data);
    free(im);
    return 4;
  }

  // write image to disk
  FILE *fp = fopen(outputname, "wb");
  if (!fp) {
    fprintf(stderr, "Uh oh. Output file failed to open!\n");
    free(im->data);
    free(im);
    return 7;
  }
  int num_pixels_written = write_ppm(fp, im);
  fclose(fp);
  printf("Image created with %d pixels.\n", num_pixels_written);

  // clean up!
  free(im->data);  // releases the pixel array
  free(im);        // releases the image itself

  return 0;

}
