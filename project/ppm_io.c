// Nathaniel Eisenberg neisenb3
// Glen Yin jyin19
// ppm_io.c
// 601.220, Spring 2019

#include <assert.h>
#include <stdlib.h>
#include <string.h>
#include "ppm_io.h"



/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image * read_ppm(FILE *fp) {

  // check that fp is not NULL
  assert(fp);

  // allocate space for an Image
  Image * im = malloc(sizeof(Image));
  if (!im) {
    fprintf(stderr, "Uh oh. Image allocation failed!\n");
    return NULL;
  }

  int parse;
  char filetype[2];
  parse = fscanf(fp, "%s", filetype);
  if (!parse) {
    fprintf(stderr, "Error: failure to read input\n");
    free(im);
    return NULL;
  }
  if (strcmp(filetype, "P6")) {
    fprintf(stderr, "Error: input file is not the correct type of image\n");
    free(im);
    return NULL;
  }

  char line[100];
  parse = fscanf(fp, " %[^\n]s", line);
  if (!parse) {
    fprintf(stderr, "Error: failure to read input\n");
    free(im);
    return NULL;
  }
  if (line[0] == '#') {
    parse = fscanf(fp, " %d %d", &(im->cols), &(im->rows));
  } else {
    parse = sscanf(line, "%d %d", &(im->cols), &(im->rows));
  }
  if (parse != 2) {
    fprintf(stderr, "Error: failure to read dimensions\n");
    free(im);
    return NULL;
  }

  int color;
  parse = fscanf(fp, " %d\n", &color);
  if (!parse) {
    fprintf(stderr, "Error: failure to read input\n");
    free(im);
    return NULL;
  }
  if (color != 255) {
    fprintf(stderr, "Error: file uses color base that is not 255\n");
    free(im);
    return NULL;
  }

  // allocate space for array of Pixels
  Pixel *pix = malloc(sizeof(Pixel) * im->rows * im->cols);
  if (!pix) {
    fprintf(stderr, "Uh oh. Pixel array allocation failed!\n");
    free(im);
    return NULL;
  }

  // let data field of Image point to the new array
  im->data = pix;

  int pixels_read = fread(pix, sizeof(Pixel), im->rows * im->cols, fp);
  if (pixels_read != im->rows * im->cols) {
    fprintf(stderr, "Error: failure to read pixels");
    free(im->data);
    free(im);
    return NULL;
  }

  return im;

}


/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp);

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}

