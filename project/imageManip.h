// Nathaniel Eisenberg neisenb3
// Glen Yin jyin19
// imageManip.h
// 601.220, Spring 2019


#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <stdio.h>
#include "ppm_io.h"

// swaps the rbg values
void swap(Image* img);

// alter the brightness of the image
void bright(Image* img, int value);

// inverts the image by subracting its color value from 255
void invert(Image* img);

// grays the image by setting each rbg value to a certain weighted intensity
void gray(Image* img);

// crops the image given the cordinates of top left and bottom right corner
void crop(Image* img, int tc, int tr, int bc, int br);

// blur the image using gaussian matrix with given standard deviation
void blur(Image* img, double sig);

// return a new Pixel with gaussian matrix applied to
// all adjacent pixels around the given coordinate
Pixel blurPix(Image* img, int i, int j, double *gaus, int N);

// converts a double value, constricted to 0-255, to an unsigned char
unsigned char cvtchar(double val);

// detects large changes in color intensity in the
// blurred-gray image to only show large changes in color intensity
void edges(Image* img, double sigma, int threshhold);

#endif
